package example;

import com.sun.istack.NotNull;

import javax.validation.constraints.DecimalMin;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Path("/priceList")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PriceListWebServiceImpl implements PriceListWebService {

    DBConnection connection = new DBConnection();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Path("/getPrice")
    @GET
    public Response getPrice(@NotNull @QueryParam("productName") String productName, @NotNull @QueryParam("date") String date) {

        LocalDate lDate = LocalDate.parse(date, formatter);

        String query = "SELECT * FROM PRICELIST WHERE \n" +
                "FROMDATE <= '" + lDate + "' \n" +
                "AND \n" +
                "TODATE >= '" + lDate + "'\n" +
                "AND \n" +
                "PRODUCTNAME ='" + productName + "'";

        BigDecimal price = null;
        try {
            ResultSet resultSet = connection.dbData(query);
            while (resultSet.next()) {
                price = resultSet.getBigDecimal(4);
            }
            connection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (!existProduct(productName)) {
            return Response.status(404).build();//not found
        } else {
            return price == null ? Response.status(204).build() : Response.ok(price).build();
        }
    }


    @Path("/getPrices")
    @GET
    public Response getPrices(@NotNull @QueryParam("productName") String productName) {

        String query = "SELECT * FROM PRICELIST WHERE " +
                "productname = '" + productName + "' " +
                "ORDER BY fromdate";
        List<ProductPrice> prices = new ArrayList<ProductPrice>();
        try {
            ResultSet resultSet = connection.dbData(query);
            while (resultSet.next()) {
                String pName = resultSet.getString(1);
                BigDecimal pPrice = resultSet.getBigDecimal(4);
                LocalDate validFrom = resultSet.getDate(2).toLocalDate();
                LocalDate validTo = resultSet.getDate(3).toLocalDate();
                ProductPrice productPrice = new ProductPrice(pName, pPrice, validFrom, validTo);
                prices.add(productPrice);
            }
            connection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (prices.size() == 0) {
            return Response.status(404).build();
        } else {
            return Response.ok(prices).build();
        }
    }

    @Path("/setPrice")
    @GET
    public Response setPrice(@NotNull @QueryParam("productName") String productName,
                             @NotNull @DecimalMin("0") @QueryParam("price") BigDecimal price,
                             @QueryParam("fromDate") String fromDate,
                             @QueryParam("toDate") String toDate) {

        LocalDate fromD = fromDate == null ? LocalDate.parse(LocalDate.MIN.toString(), formatter) : LocalDate.parse(fromDate, formatter);
        LocalDate toD = toDate == null ? LocalDate.parse(LocalDate.MAX.toString(), formatter) : LocalDate.parse(toDate, formatter);

        ProductPrice productPrice = new ProductPrice(productName, price, fromD, toD);

        if (productPrice.getValidFrom().compareTo(productPrice.getValidTo()) > 0) {
            return Response.status(500).build();
        }
        String query = "INSERT INTO PRICELIST" +
                " VALUES ('" + productPrice.getProductName() + "', '" + productPrice.getValidFrom() +
                "', '" + productPrice.getValidTo() + "' , " + productPrice.getProductPrice() + ")";

        String existPriceQuery = "SELECT * FROM PRICELIST WHERE " +
                "FROMDATE <= '" + productPrice.getValidFrom() + "' AND " +
                "TODATE >= '" + productPrice.getValidTo() + "' AND " +
                "PRODUCTNAME = '" + productPrice.getProductName() + "'";

        String updatePriceQuery = "UPDATE PRICELIST  SET " +
                "FROMDATE  = '" + productPrice.getValidFrom() + "', TODATE = '"
                + productPrice.getValidTo() + "', PRICE ='" + productPrice.getProductPrice() + "' " +
                " WHERE FROMDATE <= '" + productPrice.getValidFrom() + "' AND TODATE >= '" + productPrice.getValidTo() +
                "' AND PRODUCTNAME = '" + productPrice.getProductName() + "'";

        BigDecimal existPrice = null;
        try {
            ResultSet resultSet = connection.dbData(existPriceQuery);
            while (resultSet.next()) {
                existPrice = resultSet.getBigDecimal(4);
            }
            if (existPrice == null) {
                connection.dbDataQuery(query);
            } else {
                connection.dbDataQuery(updatePriceQuery);
            }
            connection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Path("/stopSelling")
    @GET
    public Response stopSelling(@NotNull @QueryParam("productName") String productName,
                                @QueryParam("fromDate") String fromDate,
                                @QueryParam("toDate") String toDate) {

        LocalDate fromD = fromDate == null ? LocalDate.MIN : LocalDate.parse(fromDate, formatter);
        LocalDate toD = toDate == null ? LocalDate.MAX : LocalDate.parse(toDate, formatter);
        if (fromD.compareTo(toD) > 0) {
            return Response.status(500).build();
        }
        if (!existProduct(productName)) {
            return Response.status(404).build();
        }
        String query = "DELETE FROM PRICELIST WHERE " +
                "FROMDATE <= '" + fromD + "' AND " +
                "TODATE >= '" + toD + "' AND " +
                "PRODUCTNAME = '" + productName + "'";

        try {
            connection.dbDataQuery(query);
            connection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    boolean existProduct(String productName) {
        String queryExistProduct = "SELECT * FROM PRICELIST WHERE \n" +
                "PRODUCTNAME ='" + productName + "'";
        String productExist = null;
        try {
            ResultSet resultSet = connection.dbData(queryExistProduct);
            while (resultSet.next()) {
                productExist = resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productExist != null;
    }

}
