package example;

import org.h2.jdbcx.JdbcDataSource;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBConnection {

    private JdbcDataSource ds = new JdbcDataSource();
    private Connection conn;

    public DBConnection() {
        ds.setURL("jdbc:h2:file:C:\\Users\\User\\IdeaProjects\\relax\\Price\\src\\main\\resources\\pricelist");
        ds.setUser("sa");
        ds.setPassword("");
        try {
            InitialContext ctx = new InitialContext();
            ctx.bind("jdbc/dsName", ds);
        } catch (NamingException e) {
            e.printStackTrace();
        }
        try {
            conn = ds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() throws SQLException {
        conn.close();
    }

    public ResultSet dbData(String query) throws SQLException {
        Statement statement = ds.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
    }


    public void dbDataQuery(String query) throws SQLException {
        Statement statement = ds.getConnection().createStatement();
        statement.execute(query);
    }

    public Connection getConn() {
        return conn;
    }

    public JdbcDataSource getDs() {
        return ds;
    }
}
