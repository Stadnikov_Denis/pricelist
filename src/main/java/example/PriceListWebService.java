package example;


import com.sun.istack.NotNull;

import javax.validation.constraints.DecimalMin;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.time.LocalDate;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface PriceListWebService {


    @GET
    Response getPrice(@NotNull @QueryParam("productName") String productName, @NotNull @QueryParam("date") String date);

    @GET
    Response getPrices(@NotNull @QueryParam("productName") String productName);

    @POST
    Response setPrice(@NotNull @QueryParam("productName") String productName, @NotNull @DecimalMin("0") @QueryParam("price") BigDecimal price, @QueryParam("fromDate") String fromDate, @QueryParam("toDate") String toDate);

    @POST
    Response stopSelling(@NotNull @QueryParam("productName") String productName, @QueryParam("fromDate") String fromDate, @QueryParam("toDate") String toDate);


    class ProductPrice {
        private final String productName;
        private final BigDecimal productPrice;
        private final LocalDate validFrom;
        private final LocalDate validTo;

        public ProductPrice(String productName, BigDecimal productPrice, LocalDate validFrom, LocalDate validTo) {
            this.productName = productName;
            this.productPrice = productPrice;
            this.validFrom = validFrom;
            this.validTo = validTo;
        }

        public String getProductName() {
            return productName;
        }

        public BigDecimal getProductPrice() {
            return productPrice;
        }

        public LocalDate getValidFrom() {
            if (this.validTo == null) {
                return null;
            } else {
                return validFrom;
            }
        }

        public LocalDate getValidTo() {

            if (this.validFrom == null) {
                return null;
            } else {
                return validTo;
            }
        }
    }
}

